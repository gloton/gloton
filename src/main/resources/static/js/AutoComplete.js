$(function () {
    var URL_PREFIX = "http://localhost:8983/solr/gitems/select?q=";
    var URL_MIDDLE = "OR dc.subject:";
    var URL_SUFFIX = "&wt=json&indent=true&omitHeader=true";
    $("#query").autocomplete({
        source: function (request, response) {
            var searchString = "\"" + $("#query").val() + "\"";
            var URL = URL_PREFIX + searchString + URL_SUFFIX;
            $.ajax({
                url: URL,
                success: function (data) {
                    var docs = JSON.stringify(data.response.docs);
                    var jsonData = JSON.parse(docs);
                    response($.map(jsonData, function (value, key) {
                        return {
                            label: value["dc.title_s"]
                        };
                    }));
                },
                dataType: 'jsonp',
                jsonp: 'json.wrf'
            });
        },
        minLength: 1
    })
});