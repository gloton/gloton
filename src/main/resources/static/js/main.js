$(document).ready(function() {
	$('#context-odepa').slick({
		dots: true,
		infinite: true,
		speed: 3000,
		fade: true,
		autoplay: true,
		cssEase: 'ease-in-out',
		arrows:false
	});

	$('#slick-transversales').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			}
		]
	});

	$('#slick-destacado').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 650,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			}
		]
	});

	$('#slick-fechas').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			}
		]
	});

	$('#slick-areas').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			}
		]
	});

	$('#slick-especificos').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			}
		]
	});

	$('#slick-sponsors').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
				}
			}
		]
	});


	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		element = $(e.target).attr('aria-controls');
		$('#slick-'+element).slick('refresh');
	})


	$('body').on('click', '.js-canvasMove', function(event) {
		event.preventDefault();
		/* Act on the event */
		if($(this).attr('state') == 1) {
		$('.js-results').animate({'margin-left': '0'}, 400, function(){
			$('.js-canvasMove').removeClass('invert');
		});
		$(this).attr('state', 0);
		}else{
		$('.js-results').animate({'margin-left': '-100%'}, 400, function(){
			$('.js-canvasMove').addClass('invert');
		});
		$(this).attr('state', 1);
		}
	});

	$('body').on('click', '.js-toList', function(event) {
                setCookie("layout", 1);
		event.preventDefault();
		$('.js-toGrid').removeClass('is-active');
		$(this).addClass('is-active');
		$('#list-results, #aspect_discovery_SimpleSearch_div_search-results').children('.col-xs-12').removeClass('col-md-6');
                   $(".ocultar").show();
                $(".o-resource__title").removeClass("text-limit");
	});

	$('body').on('click', '.js-toGrid', function(event) {
                setCookie("layout", 2);
		event.preventDefault();
		$(this).addClass('is-active');
		$('.js-toList').removeClass('is-active');
		$('#list-results').children('.col-xs-12').addClass('col-md-6');
                  $(".ocultar").hide();
                $(".o-resource__title").addClass("text-limit");
	});
        
        var cookie = getCookie("layout");
        if (cookie === "" || cookie === 1) {
            $('.js-toGrid').removeClass('is-active');
            $(".js-toList").addClass('is-active');
            $('#list-results, #aspect_discovery_SimpleSearch_div_search-results').children('.col-xs-12').removeClass('col-md-6');
            $(".ocultar").show();
                $(".o-resource__title").removeClass("text-limit");
        } else if (cookie === 2) {
            $(".js-toGrid").addClass('is-active');
            $('.js-toList').removeClass('is-active');
            $('#list-results, #aspect_discovery_SimpleSearch_div_search-results').children('.col-xs-12').addClass('col-md-6');
            $(".ocultar").hide();
            $(".o-resource__title").addClass("text-limit");
        }


});

var	heightjsresults = $('.js-results').outerHeight();
$(window).resize(function(event) {
	console.log();
	$('.js-canvas').height((heightjsresults+400)+'px');
	if ($('.js-canvasMove').length && $('.js-canvasMove').hasClass('invert')) {
		if ($(document).width() >= 993) {
			$('.js-results').css('margin-left', '0');
			$('.js-canvasMove').removeClass('invert');

		}
	}
});


var module = new Object();

module = {

	init : function() {
		this.getImages();
	},
	getImages : function () {
		$('.js-getImage').each(function(i, e) {
			var image;
			image = $(e).find('.js-image img').attr('src');
			$(e).find('.js-image').css('background-image', 'url(' + image + ')');
			return $(e).find('.js-image');
		});
	},

}

module.init();

$('[data-toggle="tooltip"]').tooltip();   


function setCookie(cname, cvalue) {
  var d = new Date();
  d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


if($(".js-results").height() >= $(".js-sidebar").height()) {
    $(".js-results").css("position", "relative");
    $(".js-sidebar").css("position", "absolute");
} else {
    $(".js-results").css("position", "absolute");
    $(".js-sidebar").css("position", "relative");
}


