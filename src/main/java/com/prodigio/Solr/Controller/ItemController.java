/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Controller;

import com.prodigio.Solr.Solr.SolrFacets;
import com.prodigio.Solr.Solr.SolrQueries;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author hernan
 */
@Controller
public class ItemController {
   
    @GetMapping("/item")
    public String details(@RequestParam(name="id", required=false, defaultValue="") String id, Model model, HttpServletRequest request) {
        
        SolrQueries results = new SolrQueries();
        QueryResponse qr = results.findById(id);
        SolrDocumentList docList = qr.getResults();
        
        QueryResponse qrFacets = results.findAll("", 1, 10, "score", "asc", "");
        SolrDocumentList docListFacets = qrFacets.getResults();
        
        model.addAttribute("title", "ficha");
        model.addAttribute("item", docList.get(0));
        model.addAttribute("results", docListFacets);
        String contextPath = request.getContextPath();
        model.addAttribute("contextPath", contextPath);
        
        
        
        
        FacetField facetSubject = qrFacets.getFacetField("dc.subject.spanish_ss");
        FacetField facetOrigen = qrFacets.getFacetField("origen.name_s");
        FacetField facetAuthor = qrFacets.getFacetField("dc.contributor.entity_ss");
        
        ArrayList<Facet> fss = new ArrayList();
        
        for(int i = 0; i<facetSubject.getValueCount();i++){
            Facet facet = new Facet();
            facet.setName(facetSubject.getValues().get(i).getName());
            facet.setCantidad(facetSubject.getValues().get(i).getCount());
            fss.add(facet);
        }
        ArrayList<Facet> fo = new ArrayList();
        for(int i = 0; i<facetOrigen.getValueCount();i++){
            Facet facet = new Facet();
            facet.setName(facetOrigen.getValues().get(i).getName());
            facet.setCantidad(facetOrigen.getValues().get(i).getCount());
            fo.add(facet);
        }
        ArrayList<Facet> fa = new ArrayList();
        for(int i = 0; i<facetAuthor.getValueCount();i++){
            Facet facet = new Facet();
            facet.setName(facetAuthor.getValues().get(i).getName());
            facet.setCantidad(facetAuthor.getValues().get(i).getCount());
            fa.add(facet);
        }
        
        model.addAttribute("facetSubject", fss);
        model.addAttribute("facetOrigen", fo);
        model.addAttribute("facetAuthor", fa);
        
        SolrFacets solrfacets = new SolrFacets();
               
        
        model.addAttribute("authorLink", solrfacets.createFacet(facetAuthor, contextPath+"/discover?filter=", "", "dc.contributor.entity_ss"));
//        model.addAttribute("authorLink", solrfacets.createFacet(facetAuthor, contextPath+"/discover?filter=", "", "dc.contributor.author_ss"));
        model.addAttribute("subjectLink", solrfacets.createFacet(facetSubject, contextPath+"/discover?filter=", "", "dc.subject_ss"));
        model.addAttribute("origenLink", solrfacets.createFacet(facetOrigen, contextPath+"/discover?filter=", "", "origen.name_s"));
        
        
        return "details";
    }
    public class Facet {

        private String nombre;
        private long cantidad;

        public String getName() {
            return nombre;
        }

        public void setName(String nombre) {
            this.nombre = nombre;
        }

        public long getCantidad() {
            return cantidad;
        }

        public void setCantidad(long cantidad) {
            this.cantidad = cantidad;
        }

    }
}
