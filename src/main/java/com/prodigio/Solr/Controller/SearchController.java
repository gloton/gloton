/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Controller;

import com.prodigio.Solr.Tools.Pagination;
import com.prodigio.Solr.Solr.SolrFacets;
import java.util.List;
import com.prodigio.Solr.Solr.SolrQueries;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author hernan
 */
@Controller
public class SearchController {

    @GetMapping("/discover")
    public String list(@RequestParam(name = "query", required = false, defaultValue = "") String query,
            @RequestParam(name = "filter", required = false, defaultValue = "") String filter,
            @RequestParam(name = "sort_by", required = false, defaultValue = "score") String sort_by,
            @RequestParam(name = "order", required = false, defaultValue = "desc") String order,
            @RequestParam(name = "rows", required = false, defaultValue = "10") int rows,
            @RequestParam(name = "page", required = false, defaultValue = "1") String pageNumber, Model model, HttpServletRequest request) {

        if (!pageNumber.matches("^((?:[1-9][0-9]*)(?:\\[0-9]+)?)$")) {
            pageNumber = "1";
        }

        int page = Integer.parseInt(pageNumber);

        SolrQueries results = new SolrQueries();
        QueryResponse qr = results.findAll(query, page, rows, sort_by, order, filter);
        SolrDocumentList docList = qr.getResults();

        int numFound = (int) docList.getNumFound();
        int totalPages = 0;
        if (numFound % 10 == 0) {
            totalPages = (numFound / 10);
        } else {
            totalPages = (numFound / 10) + 1;
        }
        int currentPage = page;

        FacetField facetSubject = qr.getFacetField("dc.subject.spanish_ss");
        FacetField facetOrigen = qr.getFacetField("origen.name_s");
//        FacetField facetAuthor = qr.getFacetField("dc.contributor.author_ss");
        FacetField facetAuthor = qr.getFacetField("dc.contributor.entity_ss");
        

        Pagination pagination = new Pagination();
        List<String> paginationItem = pagination.getPaginationItems(currentPage, totalPages);

        Map<String, Map<String, List<String>>> highlights = qr.getHighlighting();

        SpellCheckResponse spellCheckResponse = qr.getSpellCheckResponse();
//        List<Suggestion> suggestions = spellCheckResponse.getSuggestions();
        if (spellCheckResponse != null) {
            String spellcheck = spellCheckResponse.getFirstSuggestion(query);
            model.addAttribute("spellcheck", spellcheck);
        }

        String queryString = (request.getQueryString() != null ? request.getQueryString() : "");
        String currentURL = request.getRequestURL().toString() + (request.getQueryString() != null ? "?" : "") + (request.getQueryString() != null ? request.getQueryString() : "");

        model.addAttribute("titlepage", "Resultado de busqueda");
        model.addAttribute("results", docList);
        model.addAttribute("numFound", numFound);
        model.addAttribute("query", query);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("filter", filter);
        model.addAttribute("paginationItem", paginationItem);
        

        ArrayList<Facet> fss = new ArrayList();
        
        for(int i = 0; i<facetSubject.getValueCount();i++){
            Facet facet = new Facet();
            facet.setName(facetSubject.getValues().get(i).getName());
            facet.setCantidad(facetSubject.getValues().get(i).getCount());
            fss.add(facet);
        }
        ArrayList<Facet> fo = new ArrayList();
        for(int i = 0; i<facetOrigen.getValueCount();i++){
            Facet facet = new Facet();
            facet.setName(facetOrigen.getValues().get(i).getName());
            facet.setCantidad(facetOrigen.getValues().get(i).getCount());
            fo.add(facet);
        }
        ArrayList<Facet> fa = new ArrayList();
        for(int i = 0; i<facetAuthor.getValueCount();i++){
            Facet facet = new Facet();
            facet.setName(facetAuthor.getValues().get(i).getName());
            facet.setCantidad(facetAuthor.getValues().get(i).getCount());
            fa.add(facet);
        }
        
        model.addAttribute("facetSubject", fss);
        model.addAttribute("facetOrigen", fo);
        model.addAttribute("facetAuthor", fa);
        model.addAttribute("currentURL", currentURL);

        String contextPath = request.getContextPath();
        model.addAttribute("contextPath", contextPath);
        model.addAttribute("highlights", highlights);

        SolrFacets solrfacets = new SolrFacets();

        model.addAttribute("authorLink", solrfacets.createFacet(facetAuthor, contextPath + "/discover", filter, "dc.contributor.entity_ss"));
//        model.addAttribute("authorLink", solrfacets.createFacet(facetAuthor, contextPath+"/discover", filter, "dc.contributor.author_ss"));
        model.addAttribute("subjectLink", solrfacets.createFacet(facetSubject, contextPath + "/discover", filter, "dc.subject.spanish_ss"));
        model.addAttribute("origenLink", solrfacets.createFacet(facetOrigen, contextPath + "/discover", filter, "origen.name_s"));

        return "list";
    }
    public class Facet {

        private String nombre;
        private long cantidad;

        public String getName() {
            return nombre;
        }

        public void setName(String nombre) {
            this.nombre = nombre;
        }

        public long getCantidad() {
            return cantidad;
        }

        public void setCantidad(long cantidad) {
            this.cantidad = cantidad;
        }

    }

}
