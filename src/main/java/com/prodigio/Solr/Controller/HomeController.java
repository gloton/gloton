package com.prodigio.Solr.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class HomeController {
    @GetMapping("/")
    public String Home(Model model){                
        ModelAndView mav = new ModelAndView();
        model.addAttribute("title", "Descubridor de Información");
        return "index";
    }
    
    /*public String getErrorPath() {
        return "/error";
    }*/
    
    

}
