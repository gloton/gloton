/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Solr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author hernan
 */
public class SolrFacets {
    
    public List<String> createFacet(FacetField facets, String currentURL, String filter, String metadata) {
        
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromUriString(currentURL);

//        urlBuilder.replaceQueryParam("filter", filter);

        List<String> list = new ArrayList<>();
        for(Count facet: facets.getValues()) {
            
            urlBuilder.replaceQueryParam("filter", filter+metadata+":\""+facet.getName()+"\"");
            
            list.add(urlBuilder.build().toUriString());
            
        }
        return list;
        
        
    }
    
}
