/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Solr;

import java.io.IOException;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.StringUtils;

/**
 *
 * @author hernan
 */
public class SolrQueries {
    
    public QueryResponse findAll (String argument, int pageNum, int numItemsPerPage, String sort_by, String order, String filter) {
        argument = argument.replaceAll(":", "\\\\:");
        QueryResponse response = null;
        try {
            if(StringUtils.isEmpty(argument)) {
                argument="*:*";
            } 
            SolrQuery query = new SolrQuery();
            query.set("q", argument);
            query.setParam("q.op", "AND");
            
            //query.set("fq","dc.sdg:13");
            
            query.setHighlight(true).setHighlightSnippets(1);
            query.addHighlightField("bitstream_text_fulltext_txt");
            query.addHighlightField("dc.description_txt");
            query.addHighlightField("dc.description.abstract_txt");
            query.setHighlightSimplePre("<mark>");
            query.setHighlightSimplePost("</mark>");
            
            query.set("spellcheck", "on");
            
            query.addFilterQuery(filter);
            
            ORDER sort = SolrQuery.ORDER.asc;
            if(order.equals("desc")) {
                sort = SolrQuery.ORDER.desc;
            }
            
            query.setSort(sort_by, sort);
            query.setFacet(true);
            query.addFacetField("dc.subject.spanish_ss", "origen.name_s", "dc.contributor.author_ss","dc.contributor.entity_ss");
            
            query.set("facet.mincount", 1);
            query.setFacetLimit(5);
                      
            
            query.setStart((pageNum - 1) * numItemsPerPage);
            query.setRows(numItemsPerPage);
            
            
            SolrConnection con = new SolrConnection();
            HttpSolrClient solr = con.connect();
            response = solr.query(query);
            
            return response;
        } catch (SolrServerException ex) {
            System.out.println("SolrServerException error: " + ex);
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex);
        }
        return response;
    }
    
    public QueryResponse findById (String argument) {
        QueryResponse response = null;
        try {
            
            SolrQuery query = new SolrQuery();
            query.set("q","id:" + argument);
            query.addFilterQuery(argument);
            
            
            SolrConnection con = new SolrConnection();
            HttpSolrClient solr = con.connect();
            response = solr.query(query);
            
            return response;
        } catch (SolrServerException ex) {
            System.out.println("SolrServerException error: " + ex);
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex);
        }
        return response;
    }
    
//    public List<String> findMetadata(SolrDocumentList docList, String metadata) {
//        List<String> array = new ArrayList<>();
//        for (SolrDocument doc : docList) {
//            if(doc.getFieldValue(metadata) != null) {
//            array.add(doc.getFieldValue(metadata).toString());
//            } else {
//                array.add("");
//            }
//        }
//        
//        return array;
//    }
    
}
