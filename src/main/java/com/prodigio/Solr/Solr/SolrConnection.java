/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Solr;

import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;

/**
 *
 * @author Prodigio
 */
public class SolrConnection {
    public HttpSolrClient connect() {
        String urlString = "http://localhost:8983/solr/items";
//        String urlString = "http://localhost:8983/solr/item";

        HttpSolrClient solr = new HttpSolrClient.Builder(urlString).build();
        solr.setParser(new XMLResponseParser());

        return solr;
    }
}
