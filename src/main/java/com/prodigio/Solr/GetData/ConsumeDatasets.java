/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.GetData;

import com.prodigio.Solr.Tools.Date;
import com.prodigio.Solr.Solr.SolrConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hernan
 */
public class ConsumeDatasets {
    
    public void GetData(String uri, String origen, String id) {
        
        try {
            URL url = new URL(uri);
            
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Falló : Código de error HTTP : " +
                        conn.getResponseCode());
            }
            
            int flagOrigen = 0;
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader( in );
            String output = br.readLine();
            
            
            JSONObject json = new JSONObject(output);
            
            JSONArray metadata = json.getJSONArray("results");
            
            for (Object met: metadata) {
                JSONObject objMetadata = ((JSONObject) met);
                
                SolrInputDocument document = new SolrInputDocument();
                
                document.addField("id", id +"-"+ objMetadata.optString("endpoint").replace("/", "").replace("file:", ""));
                document.addField("dc.title_s", objMetadata.optString("title"));
                document.addField("dc.title_ss", objMetadata.optString("title"));
                document.addField("origen.name_s", origen);
                document.addField("origen.link_s", objMetadata.optString("link"));
                document.addField("dc.contributor.entity_ss", objMetadata.optString("user"));
                document.addField("dc.identifier.uri_ss", objMetadata.optString("user"));
                document.addField("dc.description_txt", objMetadata.optString("description"));
                
                Date date = new Date();
                
                document.addField("dc.date.created_s", date.timestampToDate(objMetadata.getLong("created_at")));
                document.addField("dc.date.created_dt", date.timestampToDate(objMetadata.getLong("created_at")));
                document.addField("lastmodified_s", date.timestampToDate(objMetadata.getLong("modified_at")));
                document.addField("lastmodified_dt", date.timestampToDate(objMetadata.getLong("modified_at")));
                
                JSONArray tags = objMetadata.getJSONArray("tags");
                
                
                for (Object tag: tags) {
                    
                    
                    document.addField("dc.subject_ss", tag);
                    
                }
                
                
                
                
                
                
                SolrConnection con = new SolrConnection();
                    
                    con.connect().add(document);
                    con.connect().commit().toString();
                    con.connect();
                
                
                
//                System.out.println(objMetadata.optString("endpoint").replace("/", "").replace("file:", ""));

            }
            
            
            

            
            
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException Excepción en la clase ConsumeDatasets: " + ex);
        } catch (IOException ex) {
            System.out.println("I/O Excepción en la clase ConsumeDatasets: " + ex);
        } catch (SolrServerException ex) {
            System.out.println("SolrServerException en la clase ConsumeDatasets: " + ex);
        }
        
    }
    
}
