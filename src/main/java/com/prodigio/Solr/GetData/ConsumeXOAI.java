package com.prodigio.Solr.GetData;

import com.prodigio.Solr.Solr.SolrConnection;
import com.prodigio.Solr.Tools.ExtractTextFromFile;
import javax.xml.parsers.DocumentBuilder;
import com.prodigio.Solr.Tools.Hash;
import com.prodigio.Solr.Tools.UrlHttps;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ConsumeXOAI {
    final String path = "/oai/request?verb=ListRecords&resumptionToken=xoai";
    public void ConsumeOai(String uri, String titulo, String id, int page, String orui) throws SAXException, XPathExpressionException, XPathExpressionException {
        String url = uri+path+"////"+page;
        
        //String url = uri;
        Hash hash = new Hash();
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);

            XPathFactory xpf = XPathFactory.newInstance();
            XPath xp = xpf.newXPath();
            XPathExpression xpe0 = xp.compile("//record");
            NodeList nl0 = (NodeList) xpe0.evaluate(doc, XPathConstants.NODESET);

            for (int k = 0; k < nl0.getLength(); k++) {

                SolrInputDocument document = new SolrInputDocument();
                document.addField("origen.id_s", id);
                document.addField("origen.name_s", titulo);
                
                XPathExpression handle = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'others']/field[@name='handle']");
                NodeList hand = (NodeList) handle.evaluate(doc, XPathConstants.NODESET);
                
                for(int i = 0; i < hand.getLength(); i++){
                    document.addField("origen.url_s", orui+"/handle/"+hand.item(i).getTextContent());
                }
                XPathExpression identifier = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'others']/field[@name='identifier']");
                NodeList iden = (NodeList) identifier.evaluate(doc, XPathConstants.NODESET);
                for(int i = 0; i < iden.getLength(); i++){
                    document.addField("id", hash.hash(iden.item(i).getTextContent()));
                }
                
                XPathExpression xpe1 = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='contributor']/element");
                NodeList nl1 = (NodeList) xpe1.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < nl1.getLength(); j++) {
                    String contributor = nl1.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if (contributor.equals("author")) {
                        XPathExpression xpce = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='contributor']/element[@name='author']//field");
                        NodeList nce = (NodeList) xpce.evaluate(doc, XPathConstants.NODESET);
                        for(int ce = 0; ce <nce.getLength();ce++){
                            String author = nce.item(ce).getTextContent();
                        document.addField("dc.contributor.author_ss", author.replaceAll("\n",""));
                        }
                    } else if (contributor.equals("advisor")) {
                        XPathExpression xpce = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='contributor']/element[@name='advisor']//field");
                        NodeList nce = (NodeList) xpce.evaluate(doc, XPathConstants.NODESET);
                        for(int ce = 0; ce <nce.getLength();ce++){
                            String advisor = nce.item(ce).getTextContent();
                            document.addField("dc.contributor.advisor_ss", advisor.replaceAll("\n",""));
                        }
                    } else if (contributor.equals("entity")) {
                        XPathExpression xpce = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='contributor']/element[@name='entity']//field");
                        NodeList nce = (NodeList) xpce.evaluate(doc, XPathConstants.NODESET);
                        for(int ce = 0; ce <nce.getLength();ce++){
                            String entity = nce.item(ce).getTextContent();
                            document.addField("dc.contributor.entity_ss", entity.replaceAll("\n",""));
                        }
                    } else if (contributor.equals("editor")) {
                        XPathExpression xpce = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='contributor']/element[@name='editor']//field");
                        NodeList nce = (NodeList) xpce.evaluate(doc, XPathConstants.NODESET);
                        for(int ce = 0; ce <nce.getLength();ce++){
                            String editor = nce.item(ce).getTextContent();
                            document.addField("dc.contributor.editor_ss", editor.replaceAll("\n",""));
                        }
                    }
                }
                XPathExpression date = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='date']/element");
                NodeList dt = (NodeList) date.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < dt.getLength(); j++) {
                    String dte = dt.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if (dte.equals("accessioned")) {
                        String accessioned = dt.item(j).getTextContent();
                        document.addField("dc.date.accessioned_s", accessioned.replaceAll("\n",""));
                    } else if (dte.equals("issued")) {
                        String issued = dt.item(j).getTextContent();
                        document.addField("dc.date.issued_s", issued.replaceAll("\n",""));
                    }
                }
                XPathExpression title = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='title']/element");
                NodeList tt = (NodeList) title.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < tt.getLength(); j++) {
                    String titl = tt.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if (titl.equals("alternative")) {
                        String alternative = tt.item(j).getTextContent();
                        document.addField("dc.title.alternative_s", alternative.replaceAll("\n",""));
                    } else {
                        String ttl = tt.item(j).getTextContent();
                        document.addField("dc.title_s", ttl.replaceAll("\n",""));
                    }
                }
                XPathExpression type = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='doctype']/element");
                NodeList tp = (NodeList) type.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < tp.getLength(); j++) {
                    String lg = tp.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if(lg.equals("es")){
                        XPathExpression dctp = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='doctype']/element[@name='es']//field[@name='value']");
                        NodeList dtp = (NodeList) dctp.evaluate(doc, XPathConstants.NODESET);
                        for(int i = 0; i < dtp.getLength();i++){
                            String doctype = dtp.item(i).getTextContent();
                            document.addField("dc.type_ss", doctype.replaceAll("\n", ""));
                        }
                    }else if(lg.equals("es_ES")){
                        XPathExpression dctp = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='doctype']/element[@name='es_ES']//field[@name='value']");
                        NodeList dtp = (NodeList) dctp.evaluate(doc, XPathConstants.NODESET);
                        for(int i = 0; i < dtp.getLength();i++){
                            String doctype = dtp.item(i).getTextContent();
                            document.addField("dc.type_ss", doctype.replaceAll("\n", ""));
                        }
                    }
                }
                XPathExpression description = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='description']/element");
                NodeList dsct = (NodeList) description.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < dsct.getLength(); j++) {
                    String dte = dsct.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if (dte.equals("abstract")) {
                        String dabstract = dsct.item(j).getTextContent();
                        document.addField("dc.description.abstract_txt", dabstract.replaceAll("\n",""));
                    } else if (dte.equals("provenance")) {
                    } else {
                        XPathExpression es = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='description']/element[@name = 'es']//field");
                        NodeList des = (NodeList) es.evaluate(doc, XPathConstants.NODESET);
                        for(int i = 0; i < des.getLength(); i++){
                            String desa = des.item(i).getTextContent();
                            document.addField("dc.description_txt", desa.replaceAll("\n", ""));
                        }
                    }
                }
                XPathExpression subject = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='subject']/element");
                NodeList pc = (NodeList) subject.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < pc.getLength(); j++) {
                    String pcs = pc.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if(pcs.equals("spanish")){
                        XPathExpression ss = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='subject']/element[@name = 'spanish']//field");
                        NodeList sse = (NodeList) ss.evaluate(doc, XPathConstants.NODESET);
                        for(int i = 0; i < sse.getLength(); i++){
                            if("-1".equals(sse.item(i).getTextContent())){
                            }else{
                            document.addField("dc.subject.spanish_ss",sse.item(i).getTextContent());
                            }
                        }
                    }else if(pcs.equals("english")){
                        XPathExpression se = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='subject']/element[@name = 'english']//field");
                        NodeList see = (NodeList) se.evaluate(doc, XPathConstants.NODESET);
                        for(int i = 0; i < see.getLength(); i++){
                            document.addField("dc.subject.english_ss",see.item(i).getTextContent());
                        }
                    }
                }
                
                XPathExpression sdg = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'dc']/element[@name='subject']//element[@name = 'sdg']//field");
                NodeList ods = (NodeList) sdg.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < ods.getLength(); j++) {
                    String value = ods.item(j).getAttributes().getNamedItem("name").getNodeValue();
                    if (value.equals("value")) {
                        String osg = ods.item(j).getTextContent();
                        document.addField("dc.sdg",osg.replaceAll("\n",""));
                    }
                }
                
                XPathExpression bundles = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']/element/field");
                NodeList bn = (NodeList) bundles.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < bn.getLength(); j++) {
                    String btp = bn.item(j).getTextContent();
                    if (btp.equals("ORIGINAL")) {
                        XPathExpression as = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']//*[contains(.,'ORIGINAL')]/element[@name='bitstreams']//field");
                        NodeList ast = (NodeList) as.evaluate(doc, XPathConstants.NODESET);
                        for (int l = 0; l < ast.getLength(); l++) {
                            String ogn = ast.item(l).getAttributes().getNamedItem("name").getNodeValue().toString();
                            if (ogn.equals("checksum") || ogn.equals("checksumAlgorithm") || ogn.equals("sid")) {
                            } else if (ogn.equals("originalName")) {
                                document.addField("bitstream_original_originalName_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("name")) {
                                document.addField("bitstream_original_original_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("format")) {
                                document.addField("bitstream_original_format_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("size")) {
                                document.addField("bitstream_original_sizeBytes_is",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("url")) {
                                document.addField("bitstream_original_retrieveLink_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            }
                        }
                    } else if(btp.equals("TEXT")){
                        XPathExpression as = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']//*[contains(.,'TEXT')]/element[@name='bitstreams']//field");
                        NodeList ast = (NodeList) as.evaluate(doc, XPathConstants.NODESET);
                        for (int l = 0; l < ast.getLength(); l++) {
                            String ogn = ast.item(l).getAttributes().getNamedItem("name").getNodeValue().toString();
                            if (ogn.equals("checksum") || ogn.equals("checksumAlgorithm") || ogn.equals("sid")) {
                            } else if (ogn.equals("originalName")) {
                               document.addField("bitstream_text_originalName_ss",ast.item(l).getTextContent());
                               // System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("name")) {
                               document.addField("bitstream_text_name_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("format")) {
                                document.addField("bitstream_text_format_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("size")) {
                                document.addField("bitstream_text_sizeBytes_is",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("url")) {
                                UrlHttps ssh = new UrlHttps();
                                String finaluri = ssh.Url(ast.item(l).getTextContent());
                                ExtractTextFromFile text = new ExtractTextFromFile();
                                document.addField("bitstream_text_fulltext_txt",text.getTextCont(finaluri));
                                document.addField("bitstream_text_retrieveLink_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            }
                        }
                    }else if(btp.equals("THUMBNAIL")){
                        XPathExpression as = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']//*[contains(.,'THUMBNAIL')]/element[@name='bitstreams']//field");
                        NodeList ast = (NodeList) as.evaluate(doc, XPathConstants.NODESET);
                        for (int l = 0; l < ast.getLength(); l++) {
                            String ogn = ast.item(l).getAttributes().getNamedItem("name").getNodeValue().toString();
                            if (ogn.equals("checksum") || ogn.equals("checksumAlgorithm") || ogn.equals("sid")) {
                            } else if (ogn.equals("originalName")) {
                                document.addField("bitstream_thumbnail_originalName_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("name")) {
                                document.addField("bitstream_thumbnail_name_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("format")) {
                                document.addField("bitstream_thumbnail_format_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("size")) {
                                document.addField("bitstream_thumbnail_sizeBytes_is",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("url")) {
                                document.addField("bitstream_thumbnail_retrieveLink_ss",ast.item(l).getTextContent());
                                //System.out.println(ast.item(l).getTextContent());
                            }
                        }
                    }
                }
                SolrConnection con = new SolrConnection();
                con.connect().add(document).toString();
                con.connect().commit().toString();
                con.connect().close();
            }
            page = page + 100;
           ConsumeOai(uri, titulo, id,page,orui);
            if(page <= 100){
                ConsumeOai(uri, titulo, id, page, uri);
            }
            
        } catch (IOException | ParserConfigurationException | XPathExpressionException | SolrServerException | DOMException | SAXException e) {
        }
    }
}
