/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.GetData;


import com.prodigio.Solr.Tools.ExtractTextFromFile;
import com.prodigio.Solr.Solr.SolrConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hernan
 */
public class Consume {
    
    private static final String path = "/rest";
    
    public void GetData(String uri, String origen, String id, int offset, int limit) {
        try {
            String restUrl = uri + path + "/items?expand=all&offset=" + offset + "&limit=" + limit;
            URL url = new URL(restUrl); //your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            //conn.setRequestProperty("User-Agent", "NetBeans");
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Charset", "UTF-8");
            if (conn.getResponseCode() == 500 || conn.getResponseCode() == 504) {
                GetData(uri, origen, id, offset, limit);
            }
//            if (conn.getResponseCode() != 200) {
//                throw new RuntimeException("Falló : Código de error HTTP : " +
//                        conn.getResponseCode());
//            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader( in );
            String output = br.readLine();
            
            if (output != null && !output.equals("[]")) {
                
                JSONArray json = new JSONArray(output);
                for (Object rec: json) {
                    JSONObject obj = ((JSONObject) rec);
                    
                    
                    SolrInputDocument document = new SolrInputDocument();
                    String idtype ="uuid";
                    String rest = "";
                    
                    if(obj.has("id")) {
                        idtype = "id";
                        rest = path;
                    }
                    
                    document.addField("id", id +"-"+ obj.optString(idtype));
                    document.addField("origen.id_s", id);
                    document.addField("origen.url_s", uri+"/handle/"+obj.optString("handle"));
                    document.addField("handle_s", obj.optString("handle"));
                    document.addField("lastmodified_s", obj.optString("lastModified"));
                    document.addField("origen.name_s", origen);
                    
                    JSONArray metadata = obj.getJSONArray("metadata");
                    
                    Map<String, String> mapDateIssued = new HashMap<String, String>();
                    Map<String, String> mapTitle = new HashMap<String, String>();
                    
                    for (Object met: metadata) {
                        JSONObject objMetadata = ((JSONObject) met);
                        if(objMetadata.optString("key").equals("dc.identifier.uri")) {
                            
                                document.addField(objMetadata.optString("key")+"_ss", 
                                        objMetadata.optString("value")
                                                .replace("192.168.2.41:80", "bibliotecadigital.infor.cl")
                                                .replace("//handle", "/handle")
                                                .replace("163.247.128.53:8080", "biblioteca.simef.cl")
                                                .replace(":8080", "")
                                                .replace(":80", ""));
                            
                        } else if(objMetadata.optString("key").equals("dc.identifier")) {
                            document.addField(objMetadata.optString("key")+"_ss", 
                                        objMetadata.optString("value")
                                                .replace("192.168.2.41:80", "bibliotecadigital.infor.cl")
                                                .replace("//handle", "/handle")
                                                .replace("163.247.128.53:8080", "biblioteca.simef.cl")
                                                .replace(":8080", "")
                                                .replace(":80", ""));
                            
                            
                        } else if(objMetadata.optString("key").equals("dc.date.issued")) {
                            
                            mapDateIssued.put("value", objMetadata.optString("value"));
                            document.addField("dc.date.issued_ss", objMetadata.optString("value"));
                        } else if(objMetadata.optString("key").equals("dc.title")) {
                            
                            mapTitle.put("value", objMetadata.optString("value"));
                            document.addField("dc.title_ss", objMetadata.optString("value"));
                        }  else if(objMetadata.optString("key").equals("dc.description") || objMetadata.optString("key").equals("dc.description.abstract")) {
                            
                            document.addField(objMetadata.optString("key")+"_txt", objMetadata.optString("value"));
                        } else {
                          document.addField(objMetadata.optString("key")+"_ss", objMetadata.optString("value"));
                        }
                    }
                    document.addField("dc.date.issued_s",mapDateIssued.get("value"));
                    document.addField("dc.title_s",mapTitle.get("value"));
                    JSONArray bitstreams = obj.getJSONArray("bitstreams");
                    for (Object bit: bitstreams) {
                     JSONObject objBitstreams = ((JSONObject) bit);
                     
                     if (objBitstreams.optString("bundleName").equals("ORIGINAL")) {
                         
                        document.addField("bitstream_original_retrieveLink_ss", uri+rest+objBitstreams.optString("retrieveLink"));
                        document.addField("bitstream_original_format_ss", objBitstreams.optString("format"));
                        document.addField("bitstream_original_bundleName_ss", objBitstreams.optString("bundleName"));
                        document.addField("bitstream_original_name_ss", objBitstreams.optString("name"));
                        document.addField("bitstream_original_mimeType_ss", objBitstreams.optString("mimeType"));
                        document.addField("bitstream_original_id_ss", objBitstreams.optString(id));
                        document.addField("bitstream_original_sizeBytes_is", objBitstreams.getInt("sizeBytes"));
                        document.addField("bitstream_original_description_ss", objBitstreams.optString("description"));
                        document.addField("bitstream_original_link_ss", objBitstreams.optString("link"));
                        
                     }
                     
                     if (objBitstreams.optString("bundleName").equals("THUMBNAIL")) {
                         
                      document.addField("bitstream_thumbnail_retrieveLink_ss", uri+rest+objBitstreams.optString("retrieveLink"));
                      document.addField("bitstream_thumbnail_format_ss", objBitstreams.optString("format"));
                      document.addField("bitstream_thumbnail_bundleName_ss", objBitstreams.optString("bundleName"));
                      document.addField("bitstream_thumbnail_name_ss", objBitstreams.optString("name"));
                      document.addField("bitstream_thumbnail_mimeType_ss", objBitstreams.optString("mimeType"));
                      document.addField("bitstream_thumbnail_id_ss", objBitstreams.optString(idtype));
                      document.addField("bitstream_thumbnail_sizeBytes_is", objBitstreams.getInt("sizeBytes"));
                      document.addField("bitstream_thumbnail_description_ss", objBitstreams.optString("description"));
                      document.addField("bitstream_thumbnail_link_ss", objBitstreams.optString("link"));

                     }
                     if (objBitstreams.optString("bundleName").equals("TEXT")) {
                         
                      document.addField("bitstream_text_retrieveLink_ss", uri+rest+objBitstreams.optString("retrieveLink"));
                      document.addField("bitstream_text_format_ss", objBitstreams.optString("format"));
                      document.addField("bitstream_text_bundleName_ss", objBitstreams.optString("bundleName"));
                      document.addField("bitstream_text_name_ss", objBitstreams.optString("name"));
                      document.addField("bitstream_text_mimeType_ss", objBitstreams.optString("mimeType"));
                      document.addField("bitstream_text_id_ss", objBitstreams.optString(idtype));
                      document.addField("bitstream_text_sizeBytes_is", objBitstreams.getInt("sizeBytes"));
                      document.addField("bitstream_text_description_ss", objBitstreams.optString("description"));
                      document.addField("bitstream_text_link_ss", objBitstreams.optString("link"));
                      ExtractTextFromFile text = new ExtractTextFromFile();
                      document.addField("bitstream_text_fulltext_txt", text.getTextCont(uri+rest+objBitstreams.optString("retrieveLink")));

                     }
                    }
                    
                    SolrConnection con = new SolrConnection();
                    conn.disconnect();
                    con.connect().add(document).toString();
                    con.connect().commit().toString();
                    con.connect().close();
                    
                   }
                    
                    offset = offset + limit;
                    
                    GetData(uri, origen, id, offset, limit);
                    
                }
                    
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException Excepción en la clase Consume: " + ex);
        } catch (ProtocolException ex) {
            System.out.println("ProtocolException Excepción en la clase Consume: " + ex);
        } catch (IOException ex) {
            System.out.println("IOException Excepción en la clase Consume: " + ex);
        } catch (SolrServerException ex) {
            System.out.println("SolrServerException Excepción en la clase Consume: " + ex);
        } 
    }
}
