/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Config;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

/**
 *
 * @author hernan
 */
@Component
public class ScheduledTasks {
    //segundos, min, horas, dias, mes, dia de la semana
    @Scheduled(cron = "0 15 16 * * *")
    public void scheduleTaskWithCronExpression() throws SAXException {
        JsonProperties json = new JsonProperties();
        json.getSitesConfig();
    }
    
}
    