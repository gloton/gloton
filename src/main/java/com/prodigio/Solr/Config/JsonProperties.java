/*
 * To change this license header, choose License Headers in Project JsonProperties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Config;

import com.prodigio.Solr.GetData.Consume;
import com.prodigio.Solr.GetData.ConsumeDatasets;
import com.prodigio.Solr.GetData.ConsumeXOAI;
import com.prodigio.Solr.Main;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.ResourceUtils;
import org.xml.sax.SAXException;

/**
 *
 * @author hernan
 */
public class JsonProperties {

    public void getSitesConfig() throws SAXException {

        Consume consume = new Consume();
        ConsumeDatasets consumeDatasets = new ConsumeDatasets();
        ConsumeXOAI coai = new ConsumeXOAI();

        try {
            File file = ResourceUtils.getFile("classpath:config-sites.json");
            String content = new String(Files.readAllBytes(file.toPath()));

            JSONArray json = new JSONArray(content);
            for (Object recJson : json) {
                JSONObject objJson = ((JSONObject) recJson);

                /*JSONArray other = objJson.getJSONArray("other");

                for (Object recOther : other) {
                    JSONObject objOther = ((JSONObject) recOther);
                    JSONArray rest = objOther.getJSONArray("rest");
                    for (Object recRest : rest) {
                        JSONObject objRest = ((JSONObject) recRest);
                        String url = objRest.optString("url");
                        String id = objRest.optString("id");
                        String title = objRest.optString("title");
                        consumeDatasets.GetData(url, title, id);
                    }
                }*/

                JSONArray dspace = objJson.getJSONArray("dspace");

                for (Object recDspace : dspace) {
                    JSONObject objDspace = ((JSONObject) recDspace);

                    /*JSONArray rest = objDspace.getJSONArray("rest");

                    for (Object recRest : rest) {
                        JSONObject objRest = ((JSONObject) recRest);
                        String url = objRest.optString("url");
                        String id = objRest.optString("id");
                        String title = objRest.optString("title");
                        consume.GetData(url, title, id, 0, 100);

                    }*/
                    JSONArray oai = objDspace.getJSONArray("oai");
                    for (Object recOai : oai) {
                        JSONObject objOai = ((JSONObject) recOai);
                        String url = objOai.optString("url");
                        String id = objOai.optString("id");
                        String title = objOai.optString("title");
                        try {
                            coai.ConsumeOai(url,title,id,0,url);
                        } catch (XPathExpressionException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException Excepción en la clase JsonProperties: " + ex);
        } catch (IOException ex) {
            System.out.println("IOException Excepción en la clase JsonProperties: " + ex);
        }
    }

}
