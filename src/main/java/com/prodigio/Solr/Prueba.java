/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr;

import com.prodigio.Solr.GetData.Consume;
import java.io.BufferedReader;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.*;

/**
 *
 * @author hernan
 */
public class Prueba {

    public static void main(String args[]) throws SAXException, XPathExpressionException, XPathExpressionException {
        //String url = "http://repositorio.uchile.cl/oai/request?verb=ListRecords&resumptionToken=xoai////0";
        String url = "https://repositorio.cepal.org/oai/request?verb=ListRecords&resumptionToken=xoai////40000";
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);

            XPathFactory xpf = XPathFactory.newInstance();
            XPath xp = xpf.newXPath();
            XPathExpression xpe0 = xp.compile("//record");
            NodeList nl0 = (NodeList) xpe0.evaluate(doc, XPathConstants.NODESET);

            for (int k = 0; k < nl0.getLength(); k++) {
                System.out.println("Item numero: " + (k + 1));
                //----------------------------------
                //
                //----------------------------------
                //XPathExpression bundles = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']/element/element//field");
                XPathExpression bundles = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']/element/field");
                NodeList bn = (NodeList) bundles.evaluate(doc, XPathConstants.NODESET);
                for (int j = 0; j < bn.getLength(); j++) {
                    System.out.println(bn.item(j).getTextContent());
                    String btp = bn.item(j).getTextContent();
                    if (btp.equals("ORIGINAL")) {
                        XPathExpression as = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']//*[contains(.,'ORIGINAL')]/element[@name='bitstreams']//field");
                        NodeList ast = (NodeList) as.evaluate(doc, XPathConstants.NODESET);
                        for (int l = 0; l < ast.getLength(); l++) {
                            String ogn = ast.item(l).getAttributes().getNamedItem("name").getNodeValue().toString();
                            if (ogn.equals("checksum") || ogn.equals("checksumAlgorithm") || ogn.equals("sid")) {
                            } else if (ogn.equals("originalName")) {
                                //document.addField("bitstream_original_originalName_ss",original.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("name")) {
                                //document.addField("bitstream_original_original_ss",original.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("format")) {
                                //document.addField("bitstream_original_format_ss",original.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("size")) {
                                //document.addField("bitstream_original_sizeBytes_is",original.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("url")) {
                                //document.addField("bitstream_original_retrieveLink_ss",original.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            }
                        }
                    } else if(btp.equals("TEXT")){
                        XPathExpression as = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']//*[contains(.,'TEXT')]/element[@name='bitstreams']//field");
                        NodeList ast = (NodeList) as.evaluate(doc, XPathConstants.NODESET);
                        for (int l = 0; l < ast.getLength(); l++) {
                            String ogn = ast.item(l).getAttributes().getNamedItem("name").getNodeValue().toString();
                            if (ogn.equals("checksum") || ogn.equals("checksumAlgorithm") || ogn.equals("sid")) {
                            } else if (ogn.equals("originalName")) {
                                //document.addField("bitstream_text_originalName_ss",txt.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("name")) {
                               //document.addField("bitstream_text_name_ss",txt.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("format")) {
                                //document.addField("bitstream_text_format_ss",txt.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("size")) {
                                //document.addField("bitstream_text_sizeBytes_is",txt.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("url")) {
                                //document.addField("bitstream_text_retrieveLink_ss",txt.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            }
                        }
                    }else if(btp.equals("THUMBNAIL")){
                        XPathExpression as = xp.compile("//record[" + (k + 1) + "]/metadata//element[@name = 'bundles']//*[contains(.,'THUMBNAIL')]/element[@name='bitstreams']//field");
                        NodeList ast = (NodeList) as.evaluate(doc, XPathConstants.NODESET);
                        for (int l = 0; l < ast.getLength(); l++) {
                            String ogn = ast.item(l).getAttributes().getNamedItem("name").getNodeValue().toString();
                            if (ogn.equals("checksum") || ogn.equals("checksumAlgorithm") || ogn.equals("sid")) {
                            } else if (ogn.equals("originalName")) {
                                //document.addField("bitstream_thumbnail_originalName_ss",tmb.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("name")) {
                                //document.addField("bitstream_thumbnail_name_ss",tmb.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("format")) {
                                //document.addField("bitstream_thumbnail_format_ss",tmb.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("size")) {
                                //document.addField("bitstream_thumbnail_sizeBytes_is",tmb.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            } else if (ogn.equals("url")) {
                                //document.addField("bitstream_thumbnail_retrieveLink_ss",tmb.item(i).getTextContent());document.addField("bitstream_text_originalName_ss",txt.item(i).getTextContent());
                                System.out.println(ast.item(l).getTextContent());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
