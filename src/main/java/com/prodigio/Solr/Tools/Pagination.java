package com.prodigio.Solr.Tools;

import java.util.ArrayList;
import java.util.List;

public class Pagination {
    public List<String> getPaginationItems(int currentPage, int totalPages) {
        
        
        List<String> array = new ArrayList<>();
        
        int totalrow = totalPages;
        int m = totalrow-3;
        
        int start=currentPage-3;
        int rows=currentPage+3;
        if(currentPage<4) {
            start=1;
        }
        
        if(currentPage>m) {
            rows=totalrow;
        }
        
        if(currentPage>4) {
            array.add("1");
            array.add("...");
        }
       for(int i = start;i <= rows; i++) {
         array.add(Integer.toString(i));
       }
       if(currentPage<m) {
           array.add("...");
           array.add(Integer.toString(totalrow));
       }
         return array;
    }
}
