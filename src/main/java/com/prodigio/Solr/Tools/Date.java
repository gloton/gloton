/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Tools;

import java.text.SimpleDateFormat;

/**
 *
 * @author hernan
 */
public class Date {
    
    public String timestampToDate(long unix_seconds) {
        java.util.Date date = new java.util.Date(unix_seconds*1000L);
        SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String java_date = jdf.format(date);
        return java_date;
    }
    
}
