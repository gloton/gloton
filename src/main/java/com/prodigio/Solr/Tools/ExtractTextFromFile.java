/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.Solr.Tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.stream.Collectors;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author hernan
 */
public class ExtractTextFromFile {
    
    public String getTextCont(String textUrl) {
        String result="";
        BufferedReader in;
        try {
            URL url = new URL(textUrl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            //Con replaceAll se eliminan caracteres extraños que generan errores en Solr
            result = in.lines().collect(Collectors.joining(System.lineSeparator())).replaceAll("[\n\r\f\t]", " ").replaceAll("[\u0003\u001b\u0014\u0011\u00038QLGDV\u0003GH\u000f\u0003WHPSHUDWXUD\u0013\u001c\u0018]","");
            in.close();
        } catch (MalformedURLException ex) {
            System.out.println("Malformed URL: " + ex);
        } catch (IOException ex) {
            System.out.println("I/O Error: " + ex);
        }
        return result;

    }
    
}
